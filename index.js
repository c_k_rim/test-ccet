"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const Server_1 = require("./server/Server");
const packageJSON = JSON.parse(fs.readFileSync('./package.json'));
const server = new Server_1.Server(packageJSON);
//# sourceMappingURL=index.js.map