const fs = require('fs');

import {Server} from "./server/Server";

const packageJSON = JSON.parse(fs.readFileSync('./package.json'));

const server = new Server(packageJSON);
