var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export class GUI {
    constructor(serverHost, rootElement) {
        this.responseCounter = 0;
        this.timeLabel = '';
        this.serverHost = serverHost;
        this.rootElement = rootElement;
        this.transport = {
            http: new HttpTransport(this.serverHost),
            socket: new SocketTransport(this.serverHost, this.onResponse.bind(this))
        };
        this.grubButton = rootElement.querySelector('#grab');
        if (this.grubButton) {
            this.grubButton.addEventListener('click', this.grab.bind(this));
        }
        else {
            throw new Error(`Error in DOM`);
        }
    }
    grab() {
        return __awaiter(this, void 0, void 0, function* () {
            this.timeLabel = `response ${++this.responseCounter}`;
            this.grubButton.disabled = true;
            console.time(this.timeLabel);
            try {
                const data = this.takeInputs();
                if (!this.transport[data.transport]) {
                    throw new Error(`Unknown transport: ${data.transport}`);
                }
                const result = yield this.transport[data.transport].getResult(data);
                this.onResponse(result);
            }
            catch (e) {
                console.error(e);
                this.grubButton.disabled = false;
                console.timeEnd(this.timeLabel);
                alert(e.toString());
            }
        });
    }
    onResponse(data) {
        try {
            this.render(data);
            this.grubButton.disabled = false;
            console.timeEnd(this.timeLabel);
        }
        catch (e) {
            console.error(e);
            this.grubButton.disabled = false;
            console.timeEnd(this.timeLabel);
            alert(e.toString());
        }
    }
    render(data) {
        const resultEl = this.rootElement.querySelector('#result');
        if (!resultEl) {
            throw new Error(`Error in DOM`);
        }
        const cssEl = resultEl.querySelector('#tab-one-panel'), jsEl = resultEl.querySelector('#tab-four-panel');
        if (!cssEl || !jsEl) {
            throw new Error(`Error in DOM`);
        }
        if (typeof data.css !== "object" || typeof data.js !== "object") {
            throw new Error(`Error in response structure: ${JSON.stringify(data)}`);
        }
        cssEl.innerHTML = '';
        jsEl.innerHTML = '';
        if (data.error) {
            throw new Error(`Error from server: ${data.error}`);
        }
        data.css.forEach(css => {
            const p = document.createElement('p');
            p.innerText = css;
            cssEl.appendChild(p);
        });
        data.js.forEach(js => {
            const p = document.createElement('p');
            p.innerText = js;
            jsEl.appendChild(p);
        });
    }
    takeInputs() {
        return {
            site: this.rootElement.querySelector('input[name="site"]').value,
            transport: this.rootElement.querySelector('input[name="transport"]:checked').value,
            collector: this.rootElement.querySelector('input[name="collector"]:checked').value
        };
    }
}
class HttpTransport {
    constructor(serverHost) {
        this._name = 'http';
        this.endpoint = 'grab';
        this.serverHost = serverHost;
    }
    get name() {
        return this._name;
    }
    getResult(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = `${this.serverHost}/${this.endpoint}`;
            const response = yield fetch(url, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify(params)
            });
            if (!response.ok) {
                throw new Error(`error from ${url}`);
            }
            else {
                try {
                    return yield response.json();
                }
                catch (e) {
                    throw new Error(e);
                }
            }
        });
    }
}
class SocketTransport {
    constructor(serverHost, resolve) {
        this._name = 'socket';
        this.endpoint = 'grab';
        this.socket = io(serverHost);
        this.socket.on('connect', () => {
        });
        this.socket.on(this.endpoint, (data) => {
            resolve(data);
        });
    }
    get name() {
        return this._name;
    }
    getResult(params) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            this.socket.emit(this.endpoint, params);
        }));
    }
}
//# sourceMappingURL=main.js.map