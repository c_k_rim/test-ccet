interface IParams {
    site: string;
    transport: 'http' | 'socket';
    collector: string
}

interface ITransport {
    getResult(params: IParams): Promise<IResponseData>;

    name: string
}

interface IResponseData {
    css: string[];
    js: string[];
    error: string;
}

export class GUI {

    protected rootElement: HTMLElement;
    protected serverHost: string;

    protected responseCounter: number = 0;
    protected timeLabel: string = '';

    protected transport: { http: HttpTransport; socket: SocketTransport };
    protected grubButton: HTMLButtonElement;

    /**
     * @constructor
     * @param serverHost: string - адрес сервера
     * @param rootElement: HTMLElement - html-контейнер
     */
    constructor(serverHost: string, rootElement: HTMLElement) {
        this.serverHost = serverHost;

        /**
         * Конечно же хочется отделить бизнес-логику от представления.
         * Но делать это отдельным классом или прикручивать шаблонизатор
         * в рамках тестового задания я не буду.
         */
        this.rootElement = rootElement;

        this.transport = {
            http: new HttpTransport(this.serverHost),
            socket: new SocketTransport(this.serverHost, this.onResponse.bind(this))
        };

        this.grubButton = <HTMLButtonElement>rootElement.querySelector('#grab');

        if (this.grubButton) {
            this.grubButton.addEventListener('click', this.grab.bind(this))
        } else {
            throw new Error(`Error in DOM`);
        }
    }

    /**
     * Клик по кнопке "Grab".
     * Глушим кнопку, запускаем секундомер,
     * собираем параметры запроса.
     * Обращаемся к выбранному транспорту
     */
    protected async grab() {

        this.timeLabel = `response ${++this.responseCounter}`;
        this.grubButton.disabled = true;

        console.time(this.timeLabel);

        try {
            const data: IParams = this.takeInputs();

            if (!this.transport[data.transport]) {
                throw new Error(`Unknown transport: ${data.transport}`)
            }

            const result = await this.transport[data.transport].getResult(data);
            this.onResponse(result);

        } catch (e) {
            console.error(e);
            this.grubButton.disabled = false;
            console.timeEnd(this.timeLabel);
            alert(e.toString());
        }
    }

    /**
     * Метод, отрабатывающий на "событие"-ответ от транспорта.
     * Обёртка try-catch, запускаем кнопку, останавливаем таймер.
     * А так - рендерим пришедшие данные
     * @param data: IResponseData
     */
    protected onResponse(data: IResponseData) {
        try {
            this.render(data);
            this.grubButton.disabled = false;
            console.timeEnd(this.timeLabel);
        } catch (e) {
            console.error(e);
            this.grubButton.disabled = false;
            console.timeEnd(this.timeLabel);
            alert(e.toString());
        }
    }

    /**
     * Рендерим данные о css и js
     * @param data: IResponseData
     */
    protected render(data: IResponseData): void {
        const resultEl = this.rootElement.querySelector('#result');

        if (!resultEl) {
            throw new Error(`Error in DOM`)
        }

        const cssEl = resultEl.querySelector('#tab-one-panel'),
            jsEl = resultEl.querySelector('#tab-four-panel');

        if (!cssEl || !jsEl) {
            throw new Error(`Error in DOM`)
        }

        if (typeof data.css !== "object" || typeof data.js !== "object") {
            throw new Error(`Error in response structure: ${JSON.stringify(data)}`);
        }

        cssEl.innerHTML = '';
        jsEl.innerHTML = '';

        if (data.error) {
            throw new Error(`Error from server: ${data.error}`);
        }

        data.css.forEach(css => {
            const p = document.createElement('p');
            p.innerText = css;
            cssEl.appendChild(p);
        });

        data.js.forEach(js => {
            const p = document.createElement('p');
            p.innerText = js;
            jsEl.appendChild(p);
        })
    }

    /**
     * Собираем параметры из "выбирашки" на "морде"
     */
    protected takeInputs(): IParams {
        return {
            site: (<HTMLInputElement>this.rootElement.querySelector('input[name="site"]')).value,
            transport: <IParams['transport']>(<HTMLInputElement>this.rootElement.querySelector('input[name="transport"]:checked')).value,
            collector: (<HTMLInputElement>this.rootElement.querySelector('input[name="collector"]:checked')).value
        }
    }
}

/**
 * Общаться с сервером тупо post запросом
 */
class HttpTransport implements ITransport {

    protected _name = 'http';

    protected serverHost: string;
    protected endpoint = 'grab';

    constructor(serverHost: string) {
        this.serverHost = serverHost;
    }

    public get name(): string {
        return this._name
    }

    public async getResult(params: IParams): Promise<IResponseData> {

        const url = `${this.serverHost}/${this.endpoint}`;

        const response = await fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(params)
        });

        if (!response.ok) {
            throw new Error(`error from ${url}`)
        } else {
            try {
                return <IResponseData>await response.json();
            } catch (e) {
                throw new Error(e);
            }
        }

    }
}

/**
 * Общаться с сервером через web-сокеты.
 * Мог написать и на нативном web-sockets, но,
 * что-то чёрт дернул socket.io подключить
 */
class SocketTransport implements ITransport {

    protected _name = 'socket';

    protected endpoint = 'grab';

    // @ts-ignore
    protected socket;

    constructor(serverHost: string, resolve: (data: IResponseData) => void) {
        // @ts-ignore
        this.socket = io(serverHost);

        this.socket.on('connect', () => {
        });

        this.socket.on(this.endpoint, (data: IResponseData) => {
            resolve(data);
        })
    }

    public get name(): string {
        return this._name
    }

    public getResult(params: IParams): Promise<IResponseData> {
        return new Promise(async (resolve) => {

            this.socket.emit(this.endpoint, params)


        });
    }
}
