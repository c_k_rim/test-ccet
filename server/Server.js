"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const SocketIO = require("socket.io");
const collectors_1 = require("./collectors");
const path = require('path');
const Http = require('http');
const Express = require('express'), bodyParser = require('body-parser');
const debug = {
    server: require('debug')('Server'),
    HTTP: require('debug')('HTTP'),
    socket: require('debug')('Socket.io')
};
const endpoint = 'grab';
class Server {
    constructor(packageJSON) {
        this.collectors = {
            http: new collectors_1.StupidGrab(),
            puppeteer: new collectors_1.SmartGrab()
        };
        debug.server(`run version: ${packageJSON.version}`);
        const app = Express();
        app.use(Express.static(path.join(__dirname, '../public')));
        app.use(bodyParser.json());
        const http = Http.Server(app);
        this.config = packageJSON.config;
        http.listen(this.config.port, () => {
            debug.HTTP(`listen on http://localhost:${this.config.port}`);
            app.post(`/${endpoint}`, (req, res) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const params = req.body;
                    const data = yield this.collectors[params.collector].grab(params.site);
                    res.json(data);
                }
                catch (e) {
                    res.json({ error: e.toString() });
                }
            }));
            this.initSocket(http);
        });
    }
    initSocket(http) {
        this.io = SocketIO(http);
        debug.socket(`start on http://localhost:${http.address().port}`);
        this.io.on('connection', (ws) => {
            debug.socket(`ws #${ws.id} connected`);
            ['end', 'disconnect', 'close', 'error'].forEach(name => {
                ws.on(name, () => {
                    debug.socket(`WS #${ws.id} disconnected`);
                });
            });
            ws.on(endpoint, (params) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const data = yield this.collectors[params.collector].grab(params.site);
                    ws.emit(endpoint, data);
                }
                catch (e) {
                    ws.emit(endpoint, { error: e.toString() });
                }
            }));
        });
    }
}
exports.Server = Server;
//# sourceMappingURL=Server.js.map