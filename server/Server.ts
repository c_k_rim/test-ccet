import * as SocketIO from "socket.io";
import {StupidGrab, SmartGrab} from "./collectors";

const path = require('path');

const Http = require('http');
const Express = require('express'),
    bodyParser = require('body-parser');

const debug = {
    server: require('debug')('Server'),
    HTTP: require('debug')('HTTP'),
    socket: require('debug')('Socket.io')
};

const endpoint = 'grab';

interface IParams {
    site: string;
    transport: 'http' | 'socket';
    collector: string
}


export class Server {

    protected io: SocketIO.Server;

    protected config;

    protected collectors = {
        http: new StupidGrab(),
        puppeteer: new SmartGrab()
    };

    constructor(packageJSON) {

        debug.server(`run version: ${packageJSON.version}`);

        const app = Express();
        app.use(Express.static(path.join(__dirname, '../public')));
        app.use(bodyParser.json());

        const http = Http.Server(app);

        this.config = packageJSON.config;

        http.listen(this.config.port, () => {
            debug.HTTP(`listen on http://localhost:${this.config.port}`);

            app.post(`/${endpoint}`, async (req, res) => {
                try {
                    const params: IParams = req.body;
                    const data = await this.collectors[params.collector].grab(params.site);
                    res.json(data);
                } catch (e) {
                    res.json({error: e.toString()})
                }
            });


            this.initSocket(http)
        });
    }

    protected initSocket(http): void {

        this.io = SocketIO(http);

        debug.socket(`start on http://localhost:${http.address().port}`);

        this.io.on('connection', (ws: SocketIO.Socket) => {

            debug.socket(`ws #${ws.id} connected`);

            ['end', 'disconnect', 'close', 'error'].forEach(name => {
                ws.on(name, () => {
                    debug.socket(`WS #${ws.id} disconnected`);
                });
            });

            ws.on(endpoint, async (params: IParams) => {
                try {
                    const data = await this.collectors[params.collector].grab(params.site);
                    ws.emit(endpoint, data);
                } catch (e) {
                    ws.emit(endpoint, {error: e.toString()});
                }
            })
        });
    }
}
