export interface IGrab {
    grab(url: string): Promise<any>
}
