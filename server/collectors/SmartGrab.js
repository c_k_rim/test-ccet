"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const puppeteer_1 = require("puppeteer");
const debug = require('debug')('SmartGrab');
class SmartGrab {
    constructor() {
        (() => __awaiter(this, void 0, void 0, function* () {
            this.browser = yield puppeteer_1.launch({
                args: [
                    '--start-maximized',
                    '--disable-dev-shm-usage',
                    '--unlimited-storage',
                    '--full-memory-crash-report',
                    '--no-sandbox'
                ],
                headless: true
            });
        }))();
    }
    grab(url) {
        return __awaiter(this, void 0, void 0, function* () {
            debug(`fetch ${url}`);
            const css = [], js = [];
            const page = yield this.browser.newPage();
            yield page.goto(url);
            const tree = yield page._client.send('Page.getResourceTree');
            for (const resource of tree.frameTree.resources) {
                if (resource.type === 'Stylesheet') {
                    css.push(resource.url);
                }
                else if (resource.type === 'Script') {
                    js.push(resource.url);
                }
            }
            yield page.close();
            return { css, js };
        });
    }
}
exports.SmartGrab = SmartGrab;
//# sourceMappingURL=SmartGrab.js.map