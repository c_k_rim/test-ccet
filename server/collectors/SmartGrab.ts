import {Browser, Page, launch as BrowserLaunch} from 'puppeteer';
import {IGrab} from "./IGrab";

const debug = require('debug')('SmartGrab');

export class SmartGrab implements IGrab {

    protected browser: Browser;

    constructor() {
        (async () => {
            this.browser = await BrowserLaunch({
                args: [
                    '--start-maximized',
                    '--disable-dev-shm-usage',
                    '--unlimited-storage',
                    '--full-memory-crash-report',
                    '--no-sandbox'
                ],
                headless: true
            });
        })();
    }

    async grab(url) {

        debug(`fetch ${url}`);

        const css = [],
            js = [];

        const page: Page = await this.browser.newPage();

        await page.goto(url);

        const tree = await page._client.send('Page.getResourceTree');
        for (const resource of tree.frameTree.resources) {
            if (resource.type === 'Stylesheet') {
                css.push(resource.url);
            } else if (resource.type === 'Script') {
                js.push(resource.url)
            }
        }

        await page.close();

        return {css, js}
    }
}
