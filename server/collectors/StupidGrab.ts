import {IGrab} from "./IGrab";

const fetch = require('node-fetch');

const debug = require('debug')('StupidGrab');

export class StupidGrab implements IGrab {
    grab(url): Promise<any> {
        return new Promise((resolve, reject) => {
            try {

                debug(`fetch ${url}`);

                fetch(url)
                    .then(res => res.text())
                    .then(body => {
                        const css = this.parseString(body, 'css'),
                            js = this.parseString(body, 'js');

                        resolve({css, js});
                    }).catch(e => reject(e.toString()));

            } catch (e) {
                reject(e.toString());
            }
        })
    }

    parseString(str: string, find: 'js' | 'css') {
        const regex = find == 'css'
            ? /.*link.*href=["|\']?(.*[\\\|\/]?.*\.css)["|\']?.*/ig
            : /<script.*?src="(.*?)"/ig;

        const result = [];

        let res;

        while (res = regex.exec(str)) {
            if (res.length == 2 && res[1]) {
                result.push(res[1]);
            }
        }

        return result;
    }
}
